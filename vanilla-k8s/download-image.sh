#!/bin/bash

# You can check here for versions:
# https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/builds.json

LIBVIRT_IMAGE_PATH="${LIBVIRT_IMAGE_PATH:-/var/lib/libvirt/images}"
IMAGE="${IMAGE:-/var/lib/libvirt/images/vanilla-k8s-sno.qcow2}"
VERSION="${VERSION:-39.20240225.3.0}"

NAME_XZ=fedora-coreos-$VERSION-qemu.x86_64.qcow2.xz
NAME=fedora-coreos-$VERSION-qemu.x86_64.qcow2
URL=https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/$VERSION/x86_64/fedora-coreos-$VERSION-qemu.x86_64.qcow2.xz

pushd $LIBVIRT_IMAGE_PATH

set -ex

if [[ ! -f $NAME ]]; then
wget -O $NAME_XZ $URL
unxz $NAME_XZ
fi

rm -rf $IMAGE
ln -s $NAME $IMAGE

popd
