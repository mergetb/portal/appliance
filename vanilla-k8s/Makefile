# Disable built-in rules
MAKEFLAGS += --no-builtin-rules

SNO_DIR = .

########################

MACHINE_NETWORK ?= 192.168.126.0/24
CLUSTER_NETWORK ?= 10.128.0.0/14
CLUSTER_SVC_NETWORK ?= 172.30.0.0/16

LIBVIRT_IMAGE_PATH = /var/lib/libvirt/images
STREAM = stable

NET_CONFIG_TEMPLATE = $(SNO_DIR)/net.xml.template
NET_CONFIG = $(SNO_DIR)/net.xml

NET_NAME = test-net
VM_NAME = sno-test
VOL_NAME = $(VM_NAME).qcow2

WORKDIR = $(SNO_DIR)/workdir
SSH_KEY_PUB_PATH = $(WORKDIR)/key.pub
SSH_KEY_PRIV_PATH = $(WORKDIR)/key

SSH_FLAGS = -o IdentityFile=$(SSH_KEY_PRIV_PATH) \
 			-o UserKnownHostsFile=/dev/null \
 			-o StrictHostKeyChecking=no

HOST_IP = 192.168.126.10
SSH_HOST = core@$(HOST_IP)

IGNITION_CONFIG = $(SNO_DIR)/vanilla-k8s.ign
IGNITION_CONFIG_TEMPLATE = $(SNO_DIR)/vanilla-k8s.ign.template.json
IMAGE = /var/lib/libvirt/images/vanilla-k8s-sno.qcow2
VERSION=39.20240225.3.0

$(WORKDIR):
	@echo Creating workdir
	mkdir $@

$(SSH_KEY_PRIV_PATH): $(WORKDIR)
	@echo "No private key $@ found, generating a private-public pair"
	# -N "" means no password
	ssh-keygen -f $@ -N ""
	chmod 400 $@

$(SSH_KEY_PUB_PATH): $(SSH_KEY_PRIV_PATH)

.PHONY: gather checkenv clean destroy-libvirt start-iso network ssh

# $(INSTALL_CONFIG) is also PHONY to force the makefile to regenerate it with new env vars
.PHONY: $(INSTALL_CONFIG)

.PHONY: $(WORKDIR)

.SILENT: destroy-libvirt

clean: destroy-libvirt
	rm -rf $(SSH_KEY_DIR)

destroy-libvirt:
	echo "Destroying previous libvirt resources"
	NET_NAME=$(NET_NAME) \
        VM_NAME=$(VM_NAME) \
        VOL_NAME=$(VOL_NAME) \
	$(SNO_DIR)/virt-delete-sno.sh || true

# Render the libvirt net config file with the network name and host IP
$(NET_CONFIG): $(NET_CONFIG_TEMPLATE)
	sed -e 's/REPLACE_NET_NAME/$(NET_NAME)/' \
		-e 's/REPLACE_HOST_IP/$(HOST_IP)/' \
	    $(NET_CONFIG_TEMPLATE) > $@

network: destroy-libvirt $(NET_CONFIG)
	NET_XML=$(NET_CONFIG) $(SNO_DIR)/virt-create-net.sh

$(IGNITION_CONFIG): $(IGNITION_CONFIG_TEMPLATE) $(SSH_KEY_PUB_PATH)
	cat $(IGNITION_CONFIG_TEMPLATE) | jq -c ".passwd.users[0].sshAuthorizedKeys[0] |= \"`cat $(SSH_KEY_PUB_PATH)`\"" > $@
	podman run --pull=always --rm -i quay.io/coreos/ignition-validate:release - < $@

$(IMAGE):
	$(SNO_DIR)/download-image.sh

ssh:
	ssh $(SSH_FLAGS) $(SSH_HOST)

start: network $(SSH_KEY_PRIV_PATH) $(IMAGE) $(IGNITION_CONFIG)
	$(SNO_DIR)/virt-install.sh $(IGNITION_CONFIG)

start-debug: network $(SSH_KEY_PRIV_PATH) $(IMAGE) $(IGNITION_CONFIG)
	$(SNO_DIR)/virt-install-debug.sh $(IGNITION_CONFIG)

gather:
	@echo Gathering logs...
	@echo If this fails, try killing running SSH agent instances. Installer will prefer those \
over your explicitly provided key file
	$(INSTALLER_BIN) gather bootstrap \
	--bootstrap $(HOST_IP) \
	--master $(HOST_IP) \
	--key $(SSH_KEY_PRIV_PATH)
