#!/bin/bash

set -xe

sudo virsh net-define net.xml || true

sudo virsh net-start --network test-net || true
sudo virsh start sno-test

grep -q '^192.168.126.10' /etc/hosts || cat >> /etc/hosts <<EOF
192.168.126.10 mergetb.example.net
192.168.126.10 api.mergetb.example.net
192.168.126.10 git.mergetb.example.net
192.168.126.10 grpc.mergetb.example.net
192.168.126.10 launch.mergetb.example.net
192.168.126.10 jump.mergetb.example.net
EOF

export KUBECONFIG=`pwd`/kubeconfig-$VERSION
export SSH_KEY=`pwd`/key-$VERSION
export HOST_IP=192.168.126.10
export SSH_HOST=core@$HOST_IP
export SSH_OPTIONS="-o IdentitiesOnly=yes -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

set +x

until ssh -i $SSH_KEY $SSH_OPTIONS $SSH_HOST echo vm ready; do
    echo '  waiting for VM to start up...'
    sleep 5
done

timeout 5m ssh -i $SSH_KEY $SSH_OPTIONS $SSH_HOST <<EOF
until kubectl get deployments --all-namespaces=true; do
    echo '  waiting for VM to start up...'
    sleep 5
done

echo

until kubectl get deployments --all-namespaces=true | grep '0/1' > /dev/null; do
    echo 'waiting for deployments to reboot...'
    sleep 5
done

kubectl get deployments --all-namespaces=true

table=`kubectl get deployments --all-namespaces=true | awk 'NR!=1'`
namespaces=(`echo "$table" | awk '{print $1}'`)
deploys=(`echo "$table" | awk '{print $2}'`)

for i in "${!namespaces[@]}"; do
    kubectl rollout status -n "${namespaces[i]}" deployment/"${deploys[i]}"
done

kubectl get deployments --all-namespaces=true

kubectl get pods --all-namespaces=true

echo Appliance started up successfully!
EOF

sudo virsh autostart sno-test || true
sudo virsh net-autostart test-net || true
