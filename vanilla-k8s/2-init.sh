#!/bin/bash

CLUSTER_NETWORK="172.128.0.0/14"
CLUSTER_SVC_NETWORK="10.244.0.0/16"
KUBE_VERSION="1.27.11"
INGRESS_NGINX_VERSION="1.10.0"

set -ex

sed -i 's/selinux = true/selinux = false/' /etc/crio/crio.conf

# Enable crio various services
systemctl enable --now crio && systemctl enable --now kubelet && systemctl enable --now nfs-server

# Create a local registry
mkdir -p /var/lib/registry
podman run --privileged -d --name local-registry -p 5000:5000 -v /var/lib/registry:/var/lib/registry --restart=always registry:2
podman generate systemd --new --name local-registry > /etc/systemd/system/local-registry.service
systemctl enable --now local-registry

# Install kubernetes
echo "KUBELET_EXTRA_ARGS=--cgroup-driver=systemd" | tee /etc/sysconfig/kubelet

kubeadm init --config <(cat <<EOF
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
kubernetesVersion: ${KUBE_VERSION}
controllerManager:
  extraArgs:
    flex-volume-plugin-dir: "/etc/kubernetes/kubelet-plugins/volume/exec"
networking:
  serviceSubnet: ${CLUSTER_NETWORK}
  podSubnet: ${CLUSTER_SVC_NETWORK}
---
apiVersion: kubeadm.k8s.io/v1beta3
kind: InitConfiguration
nodeRegistration:
  criSocket: unix:/var/run/crio/crio.sock
EOF
)

# Copy kubeconfigs
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

mkdir -p /var/home/core/.kube
sudo cp -i /etc/kubernetes/admin.conf /var/home/core/.kube/config
sudo chown 1000:1000 /var/home/core/.kube/config

# Allow pods to be scheduled on the master node
kubectl taint nodes --all node-role.kubernetes.io/control-plane-

# 1 CoreDNS replica
kubectl scale deployment coredns -n kube-system --replicas=1

# Install flannel and ingress-nginx
kubectl apply -f <(curl -s https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml 2>&1 | sed 's#10.244.0.0/16#${CLUSTER_SVC_NETWORK}#')
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v${INGRESS_NGINX_VERSION}/deploy/static/provider/baremetal/deploy.yaml

kubectl -n ingress-nginx rollout status deployment ingress-nginx-controller --timeout=60s || true

# Flannel requires restarting the interfaces and kubelet for some reason
# https://github.com/kubernetes/kubernetes/issues/39557
(ip link set cni0 down && ip link set flannel.1 down) || true
(ip link delete cni0 && ip link delete flannel.1) || true
systemctl restart kubelet

# We have to wait for a bit for ingress-nginx to initialize
kubectl -n ingress-nginx rollout status deployment ingress-nginx-controller --timeout=60s || kubectl get pods -n ingress-nginx | grep ingress-nginx-controller | awk '{print $1}' | xargs -- kubectl delete pod -n ingress-nginx

# Edit the ingress-nginx deployment to use the hostNetwork (because getting a LoadBalancer to work was too difficult)
# and to enable ssl-passthrough, for some of the merge services
kubectl -n ingress-nginx patch deploy ingress-nginx-controller -p \
  "$(kubectl -n ingress-nginx get deployment ingress-nginx-controller -ojson \
    | jq '.spec.template.spec += { "hostNetwork": true }' \
    | jq '.spec.template.spec.dnsPolicy |= "ClusterFirstWithHostNet"' \
    | jq '.spec.template.spec.containers[0].args |= .+ ["--enable-ssl-passthrough"]' \
    | jq '.spec.template.spec.priorityClassName = "system-node-critical"')"

# Delete the default NodePort service, as we are using host networking
kubectl -n ingress-nginx delete service ingress-nginx-controller

kubectl -n ingress-nginx rollout status deployment ingress-nginx-controller

# Restart networking, again
(ip link set cni0 down && ip link set flannel.1 down) || true
(ip link delete cni0 && ip link delete flannel.1) || true
systemctl restart kubelet

# Restart coredns because it likes to die after restarting networking
kubectl get pods -n kube-system | grep coredns | awk '{print $1}' | xargs -- kubectl delete pod -n kube-system

kubectl -n ingress-nginx rollout status deployment ingress-nginx-controller

# Install default IngressClass
kubectl apply -f <(cat <<EOF
apiVersion: networking.k8s.io/v1
kind: IngressClass
metadata:
  labels:
    app.kubernetes.io/component: controller
  name: nginx
  annotations:
    ingressclass.kubernetes.io/is-default-class: "true"
spec:
  controller: k8s.io/ingress-nginx
EOF
)

####

# BASE CONFIGURATION DONE

####

# HELLO WORLD TEST

# kubectl create deployment web --image=gcr.io/google-samples/hello-app:1.0
# kubectl expose deployment web --type=NodePort --port=8080

# kubectl apply -f <(cat <<EOF
# apiVersion: networking.k8s.io/v1
# kind: Ingress
# metadata:
#   name: example-ingress
#   annotations:
#     nginx.ingress.kubernetes.io/rewrite-target: /$1
# spec:
#   rules:
#     - host: hello-world.info
#       http:
#         paths:
#           - path: /
#             pathType: Prefix
#             backend:
#               service:
#                 name: web
#                 port:
#                   number: 8080
# EOF
# )

# echo "192.168.126.10  hello-world.info" >> /etc/hosts

# curl hello-world.info
