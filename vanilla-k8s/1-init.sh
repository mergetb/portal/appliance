#!/bin/bash

CRIO_VERSION="1.27.2"
KUBE_VERSION="1.27.11"

set -ex

# Install things
until rpm-ostree install cri-o-${CRIO_VERSION} ; do
    echo waiting for install turn...
    sleep 5
done

cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://pkgs.k8s.io/core:/stable:/v${KUBE_VERSION%.*}/rpm/
enabled=1
gpgcheck=1
gpgkey=https://pkgs.k8s.io/core:/stable:/v${KUBE_VERSION%.*}/rpm/repodata/repomd.xml.key
EOF

until rpm-ostree install kubelet-${KUBE_VERSION} kubeadm-${KUBE_VERSION} kubectl-${KUBE_VERSION}; do
    echo waiting for install turn...
    sleep 5
done

rpm-ostree install nfs-utils || rpm-ostree override remove nfs-utils-coreos --install nfs-utils

# Load required kernel modules
modprobe overlay && modprobe br_netfilter

# Kernel module should be loaded on every reboot
cat <<EOF > /etc/modules-load.d/crio-net.conf
overlay
br_netfilter
EOF

# Network settings
cat <<EOF > /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

sysctl --system

# Set SELinux to permissive mode
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
