#!/bin/bash

set -ex

FINAL_IGNITION_CONFIG="${FINAL_IGNITION_CONFIG:-/var/lib/libvirt/images/sno.ign}"
sudo cp "$1" "${FINAL_IGNITION_CONFIG}"
sudo chown qemu:qemu "${FINAL_IGNITION_CONFIG}"
sudo restorecon "${FINAL_IGNITION_CONFIG}"

STREAM="${STREAM:-stable}"
LIBVIRT_IMAGE_PATH="${LIBVIRT_IMAGE_PATH:-/var/lib/libvirt/images}"
VM_NAME="${VM_NAME:-sno-test}"
RAM_MB="${RAM_MB:-32768}"
DISK_GB="${DISK_GB:-100}"
CPU_CORE="${CPU_CORE:-16}"
NET_NAME="${NET_NAME:-test-net}"
OS_VARIANT="fedora-coreos-${STREAM}"
IMAGE="/var/lib/libvirt/images/vanilla-k8s-sno.qcow2"

virsh net-define net.xml || true

rm nohup.out || true
virt-install \
    --connect qemu:///system \
    -n "${VM_NAME}" \
    -r "${RAM_MB}" \
    --vcpus "${CPU_CORE}" \
    --os-variant="${OS_VARIANT}" \
    --import \
    --network=network:${NET_NAME},mac=52:54:00:ee:42:e1 \
    --graphics=none \
    --disk="size=${DISK_GB},backing_store=${IMAGE}" \
    --qemu-commandline="-fw_cfg name=opt/com.coreos/config,file=${FINAL_IGNITION_CONFIG}" \
    --noautoconsole
