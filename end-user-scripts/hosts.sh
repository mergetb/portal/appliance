#!/bin/bash

replace_host () {
    if grep -q "$2" /etc/hosts; then
        sudo sed -i "s/.* $2/$1 $2/g" /etc/hosts
    else
        sudo echo "$1 $2" >> /etc/hosts
    fi
}

IP=${1:-192.168.126.10}
MERGE_ADDR=${2:-mergetb.example.net}

replace_host $IP $MERGE_ADDR
replace_host $IP api.$MERGE_ADDR
replace_host $IP git.$MERGE_ADDR
replace_host $IP grpc.$MERGE_ADDR
replace_host $IP launch.$MERGE_ADDR
replace_host $IP jump.$MERGE_ADDR

sudo killall -HUP dnsmasq
