#!/bin/bash

export REGISTRY=localhost:5000
export REGISTRY_PATH=mergetb/portal/services/merge
export XDC_PATH=mergetb/portal/services/xdc
export TAG=latest
export DOCKER_ARGS=--no-cache
export DOCKER=podman
export DOCKER_PUSH_ARGS=--tls-verify=false

set -ex

cd services

#sudo -E ymk -j 1 tools.yml
#sudo -E ymk clean.yml
ymk build.yml
ymk everything-containers.yml || true

cd ../install

mkdir -p build/containers/{merge,xdc}
cp ../services/build/*.tar build/containers/merge/

mv build/containers/merge/{ssh-jump,xdc-base,wgd}.tar build/containers/xdc/

# build installer
go build

podman image prune -f
sudo podman image prune -f
