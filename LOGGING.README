======================================================================================
This README describes how logging observability is deployef on an appliance. To set up
use grafana for logging, just to step 5. 
======================================================================================

1. Install NFS default storage class via portal/helm/ansible/default-storage-class.yml

2. Install loki in monolithic mode. 

helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

Create values file for monolithic mode w/filesystem storage:
========================= 8< ========================
---
loki:
  commonConfig:
    replication_factor: 1
  storage:
    type: 'filesystem'
  schemaConfig:
    configs:
      - from: 2024-04-01
        store: tsdb
        object_store: s3
        schema: v13
        index:
          prefix: loki_index_
          period: 24h
  ingester:
    chunk_encoding: snappy
  tracing:
    enabled: true
  querier:
    # Default is 4, if you have enough memory and CPU you can increase, reduce if OOMing
    max_concurrent: 2

#gateway:
#  ingress:
#    enabled: true
#    hosts:
#      - host: FIXME
#        paths:
#          - path: /
#            pathType: Prefix

deploymentMode: SingleBinary
singleBinary:
  replicas: 1
  resources:
    limits:
      cpu: 3
      memory: 4Gi
    requests:
      cpu: 2
      memory: 2Gi
  extraEnv:
    # Keep a little bit lower than memory limits
    - name: GOMEMLIMIT
      value: 3750MiB

chunksCache:
  # default is 500MB, with limited memory keep this smaller
  writebackSizeLimit: 10MB

# Enable minio for storage
minio:
  enabled: true

# Zero out replica counts of other deployment modes
backend:
  replicas: 0
read:
  replicas: 0
write:
  replicas: 0

ingester:
  replicas: 0
querier:
  replicas: 0
queryFrontend:
  replicas: 0
queryScheduler:
  replicas: 0
distributor:
  replicas: 0
compactor:
  replicas: 0
indexGateway:
  replicas: 0
bloomCompactor:
  replicas: 0
bloomGateway:
  replicas: 0
========================= 8< ========================

install:
helm install --values ./values.yml loki --namespace=loki grafana/loki --create-namespace    

Confirm installation: kubectl get all -n loki

3. Install promtail:

Create promtail values:
========================= 8< ========================
config:                                        
  # publish data to loki                       
  clients:                                     
    - url: http://loki-gateway/loki/api/v1/push
      tenant_id: 1                             
========================= 8< ========================

helm upgrade -n loki --values ./loki-promtail-values.yml --install promtail grafana/promtail

4. Install grafana

Enable persistent storage via default storage class in the values.yml
========================= 8< ========================
persistence:
  type: pvc
  enabled: true
  storageClass: default
========================= 8< ========================

In the VTE, this will be dynamically provisioned NFS.

helm upgrade --values ./grafana-values.yml --install --namespace=loki loki-grafana grafana/grafana

5. Grafana password / login

kubectl get secret --namespace loki grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo


    export POD_NAME=$(kubectl get pods --namespace loki -l "app.kubernetes.io/name=grafana,app.kubernetes.io/instance=loki-grafana" -o jsonpath="{.items[0].metadata.name}")

    kubectl --namespace loki port-forward $POD_NAME 3000

or just do single command:

    kubectl -n loki port-forward deployments/grafana 3000


open http://localhost:3000 and login as `admin` using password above

6. Add loki as a grafana data source

    - login with creds as above admin/[password]
    - add data source
    - search for loki
    - set connection url to http://loki-gateway.loki.svc.cluster.local
    - add extra http header: "X-Scope-OrgID": "merge"
    - push Save & Test

7. Queries.

Queries are expressed in LogQL, Loki's query language.  See the documentation for the query language: https://grafana.com/docs/loki/latest/query/

Configure the portal apiserver to use JSON formatted logs, if it is not already. Edit the deployment and set LOGFORMAT=json. Once 
the logs are in json format, we can query on the json fields in the messages. Here is an example of an API call that is logged:

```json
{
  "grpc.component": "server",
  "grpc.method": "ActivateUser",
  "grpc.method_type": "unary",
  "grpc.recv.duration": "10.18µs",
  "grpc.request.content": {
    "username": "glawler"
  },
  "grpc.service": "portal.v1.Workspace",
  "grpc.start_time": "2024-04-30T17:09:55Z",
  "level": "info",
  "msg": "request received",
  "peer.address": "10.244.0.1:37210",
  "protocol": "grpc",
  "time": "2024-04-30T17:09:55Z"
}
```

Note there is meta API call data like API service (`portal.v1.Workspace`, `grpc.Method`) and payload content (in `grpc.request.content`). Given this, we can query API calls:

```
{component="apiserver"} | json | grpc_method="ActivateUser"
```

We can query activation of specific users by looking at the grpc.request.content:

```
{component="apiserver"} | json | grpc_method="ActivateUser" | grpc_request_content_username="glawler"
```
