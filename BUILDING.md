# Building

## Openshift TL;DR

The following does not yet work smoothly, but it does work with some prodding,
merge requests welcome.

Commands might fail a few times before working, so if they fail, go ahead and retry running them.

```
git submodule update --init

# You can build the portal in a separate terminal while you run the rest of the commands
sudo ./portal-build.sh

# This takes around 30 minutes to complete
export PULL_SECRET=`cat <path-to-pull-secret>/pull-secret.txt`
sudo -E ./init-machine.sh
sudo ./openshift-prepare.sh

# You want the portal services built by now
#
# the following may work better w/out sudo and running 
#   `chown $(whoami):$(whoami) -R bootstrap-in-place-poc/
sudo ./merge-install.sh

# If you want to replace the etcd and minio containers with a local hosted contained
sudo ./local-storage.sh

# wait for all merge pods to start in all merge namespaces:
#   sudo source ./common.sh
#   sudo -E oc get pods -n merge           to all show "Running"
#   sudo -E oc get pods -n merge-auth      to all show "Running"
#   sudo -E oc get pods -n xdc             to all show "Running"

# You may have to restart the NFS server on master1 before all pods will
# start. 
sudo ssh -i bootstrap-in-place-poc/ssh-key/key core@192.168.126.10 sudo systemctl restart nfs-server.service

sudo ./install-wg.sh     # build and install the wireguard kernel module

sudo ./pack-up.sh
./collect.sh
```

These instructions assume the current Fedora release with the `@virtualization`
and `@development-tools` software groups and `ansible` package installed.

## Vanilla Kubernetes TL;DR
```
git submodule update --init

./init-vanilla-k8s.sh

sudo ./pack-up.sh

cd vanilla-k8s
./collect.sh
```

## Vanilla Kubernetes Alternative Installs
```
# Make a new appliance that uses pre-built containers
./init-vanilla-k8s.sh

# Make a new appliance and build your own containers
sudo ./portal-build.sh
./init-vanilla-k8s.sh ./vanilla-k8s/portal-install.yml

# Update a running appliance that uses pre-built containers
./vanilla-k8s/update-vanilla-k8s.sh

# Update a running appliance and build your own containers
./vanilla-k8s/update-vanilla-k8s.sh ./vanilla-k8s/portal-install.yml
```

These instructions assume the current Fedora release with the `@virtualization`
and `@development-tools` software groups and `ansible` package installed.

## Explained, for troubleshooting

### Prereqs

Packages

```shell
sudo dnf install -y @virt
sudo dnf group install -y "Development Tools"

# You might not have to do these two if you're on Fedora
sudo subscription-manager repos --enable ansible-2.9-for-rhel-8-x86_64-rpms
sudo subscription-manager repos --enable codeready-builder-for-rhel-8-x86_64-rpms

sudo dnf install -y golang-bin python3 virt-install ansible httpd-tools podman gpgme-devel bcc bcc-devel device-mapper-devel

# You'll need kubectl for the vanilla k8s install
# https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
# These instructions are for Fedora/RHEL

cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
sudo dnf install -y kubectl

wget https://gitlab.com/mergetb/devops/ymk/-/jobs/artifacts/master/raw/ymk?job=make
sudo chmod +x ymk
sudo cp ymk /usr/bin/ymk
```

Submodules

```shell
git submodule update --init
```

You may also want to checkout the latest `v1-staging` for services:
```
cd services
git checkout v1-staging
cd ..
```


Set your RedHat developer account pull secret.

```
https://cloud.redhat.com/openshift/install/pull-secret
```

```shell
export PULL_SECRET=`cat <path-to-pull-secret>/pull-secret.txt`
```

Next launch the VM and kick off the OpenShift install. This will likely take 30 minutes or more to complete.

```shell
sudo -E ./init-machine.sh
```

This will spit out messages like this for quite a while and for several distinct
intervals, **this is expected**

```shell
The connection to the server api.test-cluster.redhat.com:6443 was refused - did you specify the right host or port?
cluster not ready yet
The connection to the server api.test-cluster.redhat.com:6443 was refused - did you specify the right host or port?
cluster not ready yet
The connection to the server api.test-cluster.redhat.com:6443 was refused - did you specify the right host or port?
cluster not ready yet
```

**Warning**: If you cannot connect to the server at all (so the following messages repeatedly) instead of the connection being refused, 
there's a good chance that your OKD image isn't happy: check [Troubleshooting Section](#Troubleshooting-OKD).
```
Unable to connect to the server: dial tcp 192.168.126.10:6443: connect: no route to host
Unable to connect to the server: dial tcp 192.168.126.10:6443: connect: no route to host
Unable to connect to the server: dial tcp 192.168.126.10:6443: connect: no route to host
Unable to connect to the server: dial tcp 192.168.126.10:6443: connect: no route to host
Unable to connect to the server: dial tcp 192.168.126.10:6443: connect: no route to host
```

### Prepare OpenShift for Merge installation

```shell
sudo ./openshift-prepare.sh
```

**Warning**: I have observed ^^^ crash the authentication server, if the login
below fails you may need to kick the auth.

Detecting:
```shell
oc get pods --all-namespaces | grep auth
```
```shell
openshift-authentication-operator                  authentication-operator-76d6c98449-ktnpq                  1/1     Running            6          46m
openshift-authentication                           oauth-openshift-5bff9c448-tx4j5                           1/1     Running            0          6m50s
openshift-oauth-apiserver                          apiserver-9fcc5cb9d-msbj7                                 0/1     CrashLoopBackOff   9          33m
```

Correcting:
```shell
oc -n openshift-oauth-apiserver delete pods apiserver-9fcc5cb9d-msbj7
```

The pod should bounce back up and star serving requests again.

From a new shell (without KUBECONFIG set) Login as the developer user and grab a token
```shell
oc login -u developer -p developer https://api.test-cluster.redhat.com:6443
oc whoami -t
```

### Compiling the Portal
```shell
sudo ./portal-build.sh
```

### Install Merge

```shell
sudo -E ./merge-install.sh
```

In the container push phase, the OpenShift built-in container registry can be a
bit flaky at first, you may need to run this multiple times to get all the way
through.

Now **check your install**

```shell
oc -n merge get pods
```
```shell
NAME                          READY   STATUS              RESTARTS   AGE
apiserver-7bdb96cfb9-gn7s4    1/1     Running             0          54s
cred-cb56d6444-w7fpd          1/1     Running             0          51s
etcd-95d54bdb5-4r4ng          0/1     ContainerCreating   0          57s
git-server-64fc64dc6b-fqjmn   0/1     ContainerCreating   0          50s
identity-77d4bb67b-pgjl5      0/1     ContainerCreating   0          53s
materialize-7df8f4bb5-g5hbl   1/1     Running             0          53s
mergefs-97f5c6647-9x9bq       1/1     Running             0          52s
minio-54f6d67bcf-gsj8t        0/1     ContainerCreating   0          56s
model-6689bccff-g98b9         0/1     ContainerCreating   0          49s
ops-init-54hzd                0/1     ContainerCreating   0          44s
pops-6fbc78f587-pvnh8         0/1     ContainerCreating   0          48s
realize-86c569cfcd-b9zvc      0/1     ContainerCreating   0          49s
step-ca-7d89bbc548-zz6b7      0/1     Init:0/2            0          55s
wgsvc-74bb58986-h9lvv         1/1     Running             0          52s
xdc-569b4d44df-5tb4f          1/1     Running             0          51s
```
Eventually everything should turn to running, and `ops-init` should enter a
completed state. It's ok if there are a few failures with the `ops-init` task,
that just means the init task tried to initialize the Merge portal before the
databases or apiservers were ready.

```shell
oc -n merge get pods
```
```shell
NAME                          READY   STATUS      RESTARTS   AGE
apiserver-7bdb96cfb9-gn7s4    1/1     Running     0          2m37s
cred-cb56d6444-w7fpd          1/1     Running     0          2m34s
etcd-95d54bdb5-4r4ng          1/1     Running     0          2m40s
git-server-64fc64dc6b-fqjmn   1/1     Running     0          2m33s
identity-77d4bb67b-pgjl5      1/1     Running     0          2m36s
materialize-7df8f4bb5-g5hbl   1/1     Running     0          2m36s
mergefs-97f5c6647-9x9bq       1/1     Running     0          2m35s
minio-54f6d67bcf-gsj8t        1/1     Running     0          2m39s
model-6689bccff-g98b9         1/1     Running     0          2m32s
ops-init-54hzd                0/1     Error       0          2m27s
ops-init-tvrzs                0/1     Completed   0          54s
pops-6fbc78f587-pvnh8         1/1     Running     0          2m31s
realize-86c569cfcd-b9zvc      1/1     Running     0          2m32s
step-ca-7d89bbc548-zz6b7      1/1     Running     0          2m38s
wgsvc-74bb58986-h9lvv         1/1     Running     0          2m35s
xdc-569b4d44df-5tb4f          1/1     Running     0          2m34s
```

## Tips

#### SSH access to the machine

```shell
ssh -i bootstrap-in-place-poc/ssh-key/key core@192.168.126.10
```

#### Monitoring cluster initialization progress

```shell
source common.sh
```
```shell
oc get clusterversion
```
```shell
NAME      VERSION   AVAILABLE   PROGRESSING   SINCE   STATUS
version             False       True          11m     Working towards 4.8.0-fc.0: 413 of 678 done (60% complete)
```

**Notes on these commands during install**:
- `oc` command will fail with connection refused until the Kubernetes API server
  becomes ready.
- The OpenShift install process cycles the Kubernetes API server and
  Kubernetes operators needed to respond to many `oc` requessts frequently during an
  install. A bad response to an `oc` command is most likely just the API server
  or an operator being recycled.

Another highly useful command for observability is the following. This lists the cluster operators.
When the OpenShift install is done all operators will be marked as available.

```shell
oc get co
```
```shell
NAME                                       VERSION      AVAILABLE   PROGRESSING   DEGRADED   SINCE
authentication                             4.8.0-fc.0   False       False         False      7s
cloud-credential
cluster-autoscaler
config-operator
console
csi-snapshot-controller
dns
etcd                                       4.8.0-fc.0   Unknown     Unknown       False      9s
image-registry
ingress
insights
kube-apiserver
kube-controller-manager                                 False       False         False      7s
kube-scheduler                                          False       True          False      20s
kube-storage-version-migrator              4.8.0-fc.0   True        False         False      7s
machine-api
machine-approver
machine-config
marketplace
monitoring
network                                    4.8.0-fc.0   True        True          False      64s
node-tuning
openshift-apiserver                        4.8.0-fc.0   False       False         False      21s
openshift-controller-manager                            False       True          False      24s
openshift-samples
operator-lifecycle-manager
operator-lifecycle-manager-catalog
operator-lifecycle-manager-packageserver
service-ca                                              True        True          False      4s
storage
```

#### Web Console Access

Point your web browser to https://console-openshift-console.apps.test-cluster.redhat.com
Use `kubeadmin` credentials, the passowrd is in the file
`portal-appliance/bootstrap-in-place-poc/sno-workdir/auth/kubeadmin-password`.

## Troubleshooting

### Troubleshooting OKD

You'll want to check `nohup.out`.
If it complains about not having enough space, then you'll need to extend filesystem that it lives on to at least 200 GB.

#### Checking for Sufficient VM Space
For whatever reason, the VM image allocates 200 GiB of disk space. This can cause issues if the default image location is on a filesystem without enough space.

To check:
```
sudo virsh pool-info default
```

This will produce something like
```
Name:           default
UUID:           3ba4cf63-751b-4024-b43e-ff226d9c2fe4
State:          running
Persistent:     yes
Autostart:      yes
Capacity:       855.50 GiB
Allocation:     112.94 GiB
Available:      742.57 GiB
```

If you have at least 200 GiB of available disk space, you're probably good to go.

Otherwise, you'll have to delete the default directory and change the directory to a filesystem with enough space.

```
sudo virsh pool-destroy default
sudo virsh pool-undefine default

sudo mkdir /some/folder/path

sudo virsh pool-define-as --name default --type dir --target /some/folder/path

sudo virsh pool-autostart default
sudo virsh pool-start default
```
