#!/bin/bash

export MERGE_BASE_URI=${BASE_URI:-""}
export MERGE_BASE_URI=${MERGE_BASE_URI:-mergetb.example.net}

export CLUSTER_NETWORK="172.128.0.0/14"
export CLUSTER_SVC_NETWORK="10.244.0.0/16"
export DISK_GB=200
export CPU_CORE=16
export RAM_MB=65536

export SSH_KEY=./vanilla-k8s/workdir/key
export HOST_IP="192.168.126.10"
export SSH_HOST=core@$HOST_IP
export SSH_OPTIONS="-o IdentitiesOnly=yes -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
export IMAGE=/var/lib/libvirt/images/vanilla-k8s-sno.qcow2

export CRIO_VERSION="1.27.2"
export KUBE_VERSION="1.27.11"
export INGRESS_NGINX_VERSION="1.10.0"

export KUBECONFIG=`pwd`/vanilla-k8s/workdir/kubeconfig

export INTERNAL_REGISTRY=${INTERNAL_REGISTRY:-false}
export BUILD_APPLIANCE=${BUILD_APPLIANCE:-true}
export INSTALL_PORTAL=${INSTALL_PORTAL:-true}
export K8S_NAMESPACE=${K8S_NAMESPACE:-merge}

if [[ $INTERNAL_REGISTRY != false ]]; then
    export REGISTRY=host.internal:5000
else
    export REGISTRY=registry.gitlab.com
fi

echo
echo base uri: $MERGE_BASE_URI
echo registry: $REGISTRY
echo build appliance: $BUILD_APPLIANCE
echo install portal: $INSTALL_PORTAL

# make sure git submodules are up to date
git submodule init
git submodule update --force

sudo ./end-user-scripts/hosts.sh $HOST_IP $MERGE_BASE_URI $XDC_BASE_URI

if [[ $BUILD_APPLIANCE == true ]]; then

    echo Building the appliance

    pushd vanilla-k8s
        sudo rm -rf $IMAGE
        sudo rm -rf workdir
        sudo -E make start
        sudo chown -R $(id -u):$(id -g) .
    popd

    until ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST -- echo node ready ; do
        echo waiting for node to initialize...
        sleep 5
    done

    # use shell scripts because ansible wasn't really working on coreos
    # install packages and stuff

    set -e
    cat vanilla-k8s/1-init.sh \
        | sed 's#${CRIO_VERSION}#'"$CRIO_VERSION#g" \
        | sed 's#${KUBE_VERSION}#'"$KUBE_VERSION#g" \
        | eval ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST 'sudo -s'
    set +e

    ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST 'sudo systemctl reboot &'

    # wait for reboot
    sleep 5

    until ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST -- echo node ready ; do
        echo waiting for node to reboot...
        sleep 5
    done

    # install kubernetes and configure it slightly
    set -e
    cat vanilla-k8s/2-init.sh \
        | sed 's#${CLUSTER_SVC_NETWORK}#'"$CLUSTER_SVC_NETWORK#g" \
        | sed 's#${CLUSTER_NETWORK}#'"$CLUSTER_NETWORK#g" \
        | sed 's#${CRIO_VERSION}#'"$CRIO_VERSION#g" \
        | sed 's#${KUBE_VERSION}#'"$KUBE_VERSION#g" \
        | sed 's#${INGRESS_NGINX_VERSION}#'"$INGRESS_NGINX_VERSION#g" \
        | eval ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST 'sudo -s'
    set +e

    # copy the kubeconfig
    scp $SSH_OPTIONS -i $SSH_KEY $SSH_HOST:~/.kube/config ./vanilla-k8s/workdir/kubeconfig

    sudo virsh autostart sno-test || true
    sudo virsh net-autostart test-net || true

    ssh $SSH_OPTIONS -i $SSH_KEY $SSH_HOST 'sudo podman image prune -a -f'

    echo Appliance build complete.
    echo
    echo You may now optionally run the following command to set your kubeconfig:
    echo "ln -s `pwd`/vanilla-k8s/workdir/kubeconfig ~/.kube/config"
fi

if [[ $INSTALL_PORTAL == true ]]; then

    ln -s `pwd`/vanilla-k8s/workdir/kubeconfig ~/.kube/config || true

    echo Installing the portal

    # build dependencies for local helm charts as ansible uses them
    for d in helm/auth helm/portal helm/launch; do
        pushd $d
        helm dependency update
        popd
    done;

    pushd helm/ansible

    # grab lastest tags for defaults if deafult is not  given.
    if [[ -z $PORTAL_TAG ]]; then

        # if no connection use harcdcoded defaults
        wget -q --spider https://gitlab.com
        if [ $? -eq 0 ]; then
            export PORTAL_TAG=$(git -c 'versionsort.suffix=-' ls-remote --tags --sort='v:refname' https://gitlab.com/mergetb/portal/services | grep -v '{}' | tail --lines=-1 | cut --delimiter='/' --fields=3)
        else
            export PORTAL_TAG="v1.3.1"
        fi
    fi
    if [[ -z $LAUNCH_TAG ]]; then
        # if no connection use harcdcoded defaults
        wget -q --spider https://gitlab.com
        if [ $? -eq 0 ]; then
            export LAUNCH_TAG=$(git -c 'versionsort.suffix=-' ls-remote --tags --sort='v:refname' https://gitlab.com/mergetb/portal/launch | grep -v '{}' | grep -v RC | tail --lines=-1 | cut --delimiter='/' --fields=3)
        else
            export LAUNCH_TAG="v1.1.34"
        fi
    fi

    export PORTAL_FQDN=${MERGE_BASE_URI}

    # email/comms config
    # to see an example of how to tunnel to an email server, see:
    # https://gitlab.com/mergetb/operations/-/wikis/Testing-Email-in-an-VTE-by-tunnelling-to-the-SPHERE-postfix-server
    export COMMUNICATIONS_ENABLED=${COMMUNICATIONS_ENABLED:-false}  # no one probably wants email unless they are testing it.
    export SMTP_HOST=${SMTP_HOST:-"host.internal"}  # required
    export SMTP_PORT=${SMTP_PORT:-"5587"}  # required
    export SMTP_USER=${SMTP_USER}
    export SMTP_PW=${SMTP_PW}
    export PORTAL_EMAIL_FROM=${PORTAL_EMAIL_FROM:-admin@$PORTAL_FQDN}  # required

    cat extravars.yml.tpl | ./generate.sh
    set -e
    ansible-playbook -i inventory -e @extravars.yml merge-portal.yml
    popd

    # kubectl get pods -n merge | grep ops-init | awk '{print $1}' | xargs -- kubectl delete pod -n merge
    # kubectl get pods --all-namespaces=true
    #
    echo Portal install complete!
    echo
fi
