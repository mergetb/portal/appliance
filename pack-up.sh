#!/bin/bash

VM=sno-test

virsh shutdown $VM

state=$(virsh list --all | grep " $VM " | awk '{ print $3}')
while ([ "$state" != "" ] && [ "$state" == "running" ]); do
    echo "Waiting for appliance to shut down"
    sleep 10
    state=$(virsh list --all | grep " $VM " | awk '{ print $3}')
done;

